<?php

use App\Http\Controllers\Users\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//>>>> AUTH >>>
Route::post('v1/auth/login' , [App\Http\Controllers\Auth\AuthController::class , 'login']);
Route::post('v1/auth/register' ,[UsersController::class, "store"]);

Route::get("/v1/test" ,  function () {
    return "ok";
} );