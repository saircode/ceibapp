<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;

class AuthController extends Controller
{
    public function login(Request $request) 
    {
        
       
        $fields = Validator::make($request->all(), [
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        //>> RESPONSE VALIDATE FIELDS ERRORS
        if ($fields->fails()) {
            return response()->json($fields->errors(), 422);
        }
        
        $user = User::where('email', $request->email)->first();
       

        // Check password
        if(!$user || !Hash::check($request->password, $user->password)) {
            $response = ['message' => 'Bad creds'];
            return response($response, 401);
        }

        Auth::login($user);

        
        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function me()
    {
        $me = Auth::user();
        return response($me , 201);
    }

    public function logout(){
        Auth::logout();
    }
}
