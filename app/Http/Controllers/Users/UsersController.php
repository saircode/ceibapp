<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Profile\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class UsersController extends Controller
{
   
    public function index()
    {
        $Users = User::select("users.*", "profiles.name as profile")
        ->join("profiles","users.profile_id", "=", "profiles.id")->latest()->get();

        return Inertia::render('Users/index', [
            "Users" => $Users
        ]);
    }

    
    public function create()
    {
        $Profiles = Profile::get();
        return Inertia::render("Users/create", [
            "Profiles" => $Profiles
        ]);
    }

   
    public function store(Request $request)
    {
        
        $User = $request->all();
        $User["password"] = bcrypt($User["password"]);

        $validator = Validator::make($User,[
            
            'name' => 'required|string',
            'phonenumber' => 'required|numeric|unique:users',            
            'password' => 'required|string',
            'profile' => 'required',
            'status' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }


        $User = User::create($User);

        return response($User , 201) ;
    }

    

   
    public function edit($id)
    {   
        $Profiles = Profile::get();
        $User = User::find($id);
        return Inertia::render("Users/create", [
            "UserSelected" => $User,
            "Profiles" => $Profiles
        ]);
    }

    
    public function update(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'profile_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $UserSelected = User::find($request->id);
        $Fails_DocNumberValidate = User::where("document_number" , $request->document_number)
        ->where("id" , "!=", $UserSelected->id)->first();

        if($Fails_DocNumberValidate){
            return response(["document_number" => ["El número de documento ya se encuentra en uso."]] , 422);
        }

        $Fails_EmailValidate = User::where("email" , $request->email)
        ->where("id" , "!=", $UserSelected->id)->first();

        if($Fails_EmailValidate){
            return response(["email" => ["El correo electrónico está en uso."]] , 422);
        }


    }

    public function updateStatus($id, $status){
        $User = User::find($id);
        $User->update([
            "status" => $status
        ]);
    }

    
    public function destroy($id)
    {
        //
    }
}
